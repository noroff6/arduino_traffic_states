// Pushbutton
const int buttonLeft = 2;
const int buttonBottom = 13;

// Pedestrians
const int walkingGreenLeft = 3;
const int walkingRedLeft = 4;
const int walkingGreenBottom = 5;
const int walkingRedBottom = 6;

// Cars
const int redRight = 7;
const int yellowRight = 8; 
const int greenRight = 9;
const int greenTop = 10;
const int yellowTop  = 11;
const int redTop  = 12;

const int analogToggle = 20;

// Time
unsigned long previousMillis = 0;        
const long interval = 2000;  

// States
int state;
int previousState;
int ledState = LOW;

void setup() {
  Serial.begin(9600);
  for(int i=3; i<13; i++)
  {
    pinMode(i , OUTPUT);
  }

  // Buttons
  pinMode(buttonLeft, INPUT_PULLUP);
  pinMode(buttonBottom, INPUT_PULLUP);

  state = redTop;

}

void loop() {
  int analogValue = analogRead(A0);
  int buttonValueLeft = digitalRead(buttonLeft);
  int buttonValueBottom = digitalRead(buttonBottom);
  

  unsigned long currentMillis = millis();

  if(buttonValueLeft == LOW || buttonValueBottom == LOW){
    if(state == greenTop){
      stateGreenTop();
    }
    if(state == yellowTop){
      stateYellowTop();
    }
    if(state == redTop){
      stateRedTop();
    }

    if(buttonValueLeft == LOW){
      state = buttonLeft;
    }
    if(buttonValueBottom == LOW){
      state = buttonBottom;
    }
  }

  if(analogValue>=985.00)
  {
    state = analogToggle;
  }

    switch(state){
      case redTop:
        redTopGreenRightOn(redTop);
        if (currentMillis - previousMillis >= interval){
          previousMillis = currentMillis;
          previousState = state;
          state = yellowTop;
        }
        break;

      case yellowTop:
      yellowLights();
      if (currentMillis - previousMillis >= interval){
          previousMillis = currentMillis;
          if(previousState == greenTop){
            state = redTop;
          }
          if(previousState == redTop){
            state = greenTop;
          }
          
        }
        break;

      case greenTop:
      greenTopRedRightOn();
      if (currentMillis - previousMillis >= interval){
          previousMillis = currentMillis;
          previousState = state;
          state = yellowTop;
          
      }
      break;

      case buttonLeft:
      buttonOn(walkingGreenLeft, walkingRedLeft);
      if (currentMillis - previousMillis >= interval){
          previousMillis = currentMillis;
          buttonOff(walkingGreenLeft, walkingRedLeft);
          state = redTop;
      }
      break;

      case buttonBottom:
      buttonOn(walkingGreenBottom, walkingRedBottom);
      if (currentMillis - previousMillis >= interval){
          previousMillis = currentMillis;
          buttonOff(walkingGreenBottom, walkingRedBottom);
          state = redTop;
      }
      break;

      case analogToggle:
      nightMode();
      if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
      digitalWrite(yellowTop, ledState);
      digitalWrite(yellowRight, ledState);

      state = previousState;
      
      }
      
    break;

}
}

void nightMode()
{
  digitalWrite(walkingRedLeft, HIGH);
  digitalWrite(walkingRedBottom, HIGH);
  for(size_t i = 5; i>13; i++)
  {
    digitalWrite(i, LOW);
  }
}


void stateGreenTop(){
  delay(interval);
  digitalWrite(greenTop, LOW);
  digitalWrite(yellowTop, HIGH);
  delay(interval);
  digitalWrite(yellowTop, LOW);
  digitalWrite(redTop, HIGH);
}

void stateYellowTop(){
  delay(interval);
  digitalWrite(yellowTop, LOW);
  digitalWrite(yellowRight, LOW);
  delay(interval);
  digitalWrite(redTop, HIGH);
  digitalWrite(redRight, HIGH);
}

void stateRedTop(){
  delay(interval);
  digitalWrite(greenRight, LOW);
  digitalWrite(yellowRight, HIGH);
  delay(interval);
  digitalWrite(yellowRight, LOW);
  digitalWrite(redRight, HIGH);
}

void buttonOn(int green, int red){
  Serial.println("WALK");
  digitalWrite(green, HIGH);
  digitalWrite(red, LOW);
  delay(interval);
}

void buttonOff(int green, int red){
  Serial.println("DON'T WALK");
  digitalWrite(green, LOW);
  digitalWrite(red, HIGH);
}

void redTopGreenRightOn(int red)
{   digitalWrite(walkingRedBottom, HIGH);
    digitalWrite(walkingRedLeft, HIGH);
  if(red == redTop)
  {
    digitalWrite(greenTop, LOW);
    digitalWrite(yellowTop, LOW);
    digitalWrite(redTop, HIGH);
    digitalWrite(redRight, LOW);
    digitalWrite(yellowRight, LOW);
    digitalWrite(greenRight, HIGH);
  }
  if(red == redRight){
    digitalWrite(greenRight, LOW);
    digitalWrite(yellowRight, LOW);
    digitalWrite(redRight, HIGH);
    digitalWrite(redTop, LOW);
    digitalWrite(yellowTop, LOW);
    digitalWrite(greenTop, HIGH);
  }

}

void yellowLights(){
  digitalWrite(redTop, LOW);
  digitalWrite(yellowTop, HIGH);
  digitalWrite(greenTop, LOW);

  digitalWrite(redRight, LOW);
  digitalWrite(yellowRight, HIGH);
  digitalWrite(greenRight, LOW);
}

void greenTopRedRightOn(){
  digitalWrite(redTop, LOW);
  digitalWrite(yellowTop, LOW);
  digitalWrite(greenTop, HIGH);

  digitalWrite(redRight, HIGH);
  digitalWrite(yellowRight, LOW);
  digitalWrite(greenRight, LOW);
}